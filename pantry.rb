require 'yaml'

class Pantry

    def initialize
        
        @mise = Mise.new

        file = 'data/newpantry.yml'

        inventory_list = YAML.load_file(file)

        inventory_list.each { |key, dict|

            dict.each { |dict_key, value|
                @mise.add(Ingredient.new(value, key))
            }
        }

    end
end
