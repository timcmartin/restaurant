class Ingredient

    attr_reader :category, :sort_order, :name, :description

    def initialize(category, sort_order, name, extra)

        @category = category
        @sort_order = sort_order
        @name = name
        @extra = extra

    end

end
