class Menu

    attr_reader :menu_items

    def initialize
        @menu_items = {}
    end

    def add(menu_item)

        @menu_items[menu_item.sort_order] = menu_item

        self

    end

    def menu_list_string

        menu_list_string = ''

        @menu_items.each do |key, value|

            if menu_list_string == ''
                menu_list_string = menu_list_string + "#{value.name}"

            elsif @menu_items.length == key
                menu_list_string = menu_list_string + " and #{value.name}"

            else
                menu_list_string = menu_list_string + ", #{value.name} "

            end

        end

        return menu_list_string

    end

    def menu_list

        menu_list = ''

        @menu_items.each do |key, value|
            if menu_list == ''
                menu_list = "\n\t#{key} - #{value.name}\n"
            else
                menu_list = menu_list + "\t#{key} - #{value.name}\n"
            end
        end

        menu_list = menu_list + "\tq - Quit\n"

        return menu_list
    end

    def menu_count
        return @menu_items.count
    end
end
