require './chef.rb'
require './mise.rb'
require './ingredient.rb'
require './sandwich.rb'    
require './waitress.rb'
require './menu_item.rb'
require './menu.rb'

@db = "./data/newpantry.yml"

chef = Chef.new(@db)
p(chef.mise)
waitress = Waitress.new

print ("Chez Martin\n")
print ("**************\n")
print ("Hello, my name is #{waitress.name}.\n\r")
print ("Today we have #{chef.dish_menu.menu_list} on the menu.\n")
print ("What would you like to order today?\n")
