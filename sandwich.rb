class Sandwich

    attr_reader :ingredients

    def initialize
        @ingredients = {}
    end

    def add(ingredient)
        @ingredients[ingredient.category] = {} unless @ingredients.has_key? ingredient.category
        @ingredients[ingredient.category][ingredient.name] = ingredient unless @ingredients[ingredient.category].has_key? ingredient.name

        self
    end

    def sandwich_description
        
        current_sandwich = ""

        if @ingredients.empty? || @ingredients["bread"] = "" || @ingredients["meat"] = ""
            current_sandwich = "Not enough ingredients to make a sandwich"

        else
            current_sandwich = "#{@ingredients["meat"]} sandwich "
            current_sandwich = current_sandwich + "on #{@ingredients["bread"]} "
            current_sandwich = current_sandwich + "with #{@ingredients["condiments"]}"

        end

        return current_sandwich.to_s

    end
end
