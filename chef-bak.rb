require 'yaml'

class Chef

    attr_reader :sandwich

    def initialize(db)

        #@SANDWICH = SANDWICH.NEW
        @mise = Mise.new

        pantry_items = YAML.load_file(db)

        self.log_data(pantry_items)
        
        pantry_items.each { |key, value|
            
            self.log_data(key)
            self.log_data(value)
            
            value.each { |sort_order, name, description|

                self.log_data(sort_order)
                self.log_data(name)
                self.log_data(description)
                
                @mise.add(Ingredient.new(key, sort_order, name, description))

            }
        }

        @mise.list_ingredients

    end

    def log_data(item)
        puts "#{item}"
    end
end
