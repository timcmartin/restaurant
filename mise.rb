class Mise

    attr_reader :ingredients

    def initialize
        @ingredients = {}
    end

    def add(ingredient)
        raise ArgumentError, "Can't add that ingredient to your mise en place." unless ingredient.instance_of?(Ingredient)
        @ingredients[ingredient.category] = {} unless @ingredients.has_key? ingredient.category
        @ingredients[ingredient.category][ingredient.name] = {} unless @ingredients[ingredient.category].has_key? ingredient.name
        @ingredients[ingredient.category][ingredient.name] = ingredient

        self
    end
    
    def to_s

        return_string = ""
        @ingredients.each do |category, value|
            value.each do |name, ingredient |
                return_string = return_string + "#{category}--#{name}--#{ingredient.sort_order}\n" 
            end
        end
        return return_string
    end
end
