require 'yaml'

class Chef

    attr_reader :sandwich
    attr_reader :dish_menu
    attr_reader :mise

    def initialize(db)

        @sandwich = Sandwich.new
        @mise = Mise.new

        pantry_items = YAML.load_file(db)

        pantry_items.each { |key, value|
            
            value.each { |sort_order, item_hash| 

                @mise.add(Ingredient.new(key, sort_order, item_hash["type"], item_hash))

            }
        }

        self.create_menu_items

    end

    def create_menu_items

        @dish_menu = Menu.new

        menu_items = YAML.load_file("./data/menu_items.yml")
        menu_items.each { |key, value|

            value.each { |sort_key, menu_value|

                @dish_menu.add(MenuItem.new(sort_key, menu_value))
                
            }
        }

    end

end
