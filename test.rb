require './chef.rb'
require './mise.rb'
require './ingredient.rb'
require './sandwich.rb'    
require './waitress.rb'
require './menu_item.rb'
require './menu.rb'

@db = "./data/newpantry.yml"

@chef = Chef.new(@db)
@waitress = Waitress.new('Sally')

print ("Chez Martin\n")
print ("**************\n")
print ("Hello, my name is #{@waitress.name}.\n\r")
print ("Today we have the following on the menu:\n")
print ("#{@chef.dish_menu.menu_list}\n")
print ("What would you like to order today?\n")
response = gets
exit if response == 'q'
