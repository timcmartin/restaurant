class MenuItem


    attr_reader :sort_order, :name

    def initialize(sort_order, name)

        @sort_order = sort_order
        @name = name

    end
end
